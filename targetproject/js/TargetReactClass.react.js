var TargetReactClass = React.createClass({
  getInitialState: function() {
    return {value: 'Type some *markdown* here!'};
  },
  handleChange: function() {
    this.setState({value: this.refs.textarea.value});
  },
  rawMarkup: function() {
    var md = 1;
    return { __html: md.render(this.state.value) };
  },
  render: function() {
    return (
      <div className="TargetReactClass">
        Testing
      </div>
    );
  }
});

ReactDOM.render(<TargetReactClass />, mountNode);



