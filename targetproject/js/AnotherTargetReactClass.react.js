var AnotherTargetReactClass = React.createClass({
  getInitialState: function() {
    return {value: 'Type some *markdown* here!'};
  },
  handleChange: function() {
    this.setState({value: this.refs.textarea.value});
  },
  rawMarkup: function() {
    var md = new Remarkable();
    return { __html: md.render(this.state.value) };
  },
  render: function() {
    return (
      <div className="AnotherTargetReactClass">
          Testing2
      </div>
    );
  }
});

ReactDOM.render(<AnotherTargetReactClass />, mountNode);



