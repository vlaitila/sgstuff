#include <targetproject/targetheader.h>

#include "mydefines.h"


#ifdef ACOMPILERFLAG
   #include "xyz.h"

#endif


int main() {
#if CFLAG_E == 0
    return E_FREE;
#elif CFLAG_E == 1
    return E_BASIC;
#else
    return E_PRO;
#endif
}
