package sgstuff;

/**
 *
 * Service REST controller.
 * @author nn
 */
@Api(value = "APIVAL01", description = "Service description")
@Controller
@RequestMapping(produces = "application/json")
public class ClassWithSwaggerAnnotations {

/*
 * ClassWithSwaggerAnnotations is a demo class that is like an API class.
 * @param number id
 * @return
 * */
    @ApiOperation(value = "Fetch an item",
        notes = "Operation id: 001 />"
        + "Used parameters: number")
    @ApiResponses({
    @ApiResponse(code = 200, message = "Successful", response = Message.class) })
    @RequestMapping(method = RequestMethod.GET, path = PATH_ROOT)
    @ResponseBody
    public ResponseEntity getFoo(
        // @formatter:off
        @PathVariable @ApiParam("Id") String number,
         Message message)
        // @formatter:on
     {
         return null;   
     }
}
