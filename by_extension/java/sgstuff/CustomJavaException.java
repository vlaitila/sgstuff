package sgstuff;

public class CustomJavaException extends Exception {
    public CustomJavaException(String msg) {
        super(msg);
    }
}