
var AnotherTargetReactClass = require('./AnotherTargetReactClass.react.js');

var HelloReactClass = React.createClass({
  render: function() {
    return <div>Hello {this.props.name}</div>;
  }
});

ReactDOM.render(
  <Hello name="World" />,
  document.getElementById('container')
);



