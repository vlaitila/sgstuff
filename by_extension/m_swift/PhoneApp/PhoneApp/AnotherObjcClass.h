//
//  AnotherObjcClass.h
//  PhoneApp
//
//  Created by Ville Laitila on 03/08/16.
//  Copyright © 2016 asdf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnotherObjcClass : NSObject
{
    
}
-(void)doSomething;
@end
