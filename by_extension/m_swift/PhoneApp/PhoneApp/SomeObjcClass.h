//
//  SomeObjcClass.h
//  PhoneApp
//
//  Created by Ville Laitila on 02/08/16.
//  Copyright © 2016 asdf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SomeObjcClass : NSObject
{
    
}
-(void)doSomething;
@end
