//
//  ViewController.m
//  PhoneApp
//
//

#import "ViewController.h"
#import "SomeObjcClass.h"
#import "AnotherObjcClass.h"

@interface ViewController ()

@end

@implementation ViewController

   AnotherObjcClass* aMemberRef;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    SomeObjcClass *anObj = [[SomeObjcClass new] init];
    int i = anObj.accessibilityViewIsModal;
    int y = aMemberRef.accessibilityFrame.size.height;

    [aMemberRef doSomething];
    [anObj doSomething];
}

@end
