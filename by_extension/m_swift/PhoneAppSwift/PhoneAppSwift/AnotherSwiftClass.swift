//
//  AnotherSwiftClass.swift
//  PhoneAppSwift
//
//  Created by Ville Laitila on 03/08/16.
//  Copyright © 2016 asdf. All rights reserved.
//

import Foundation

class AnotherSwiftClass {
    
    func doSomething() {
        NSLog("doSomething");
        
    }
}