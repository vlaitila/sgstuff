//
//  ViewController.swift
//  PhoneAppSwift
//
//  Created by Ville Laitila on 03/08/16.
//  Copyright © 2016 asdf. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var aMemberRef = AnotherSwiftClass();

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        let c = MySwiftClass();
        c.doSomething();
    }


}

