#include "interfacex.h"
#include "interfacecaller.h"

#ifndef IMPLEMENTATION_H
#define IMPLEMENTATION_H


// implementation for the interface
class Implementation : public InterfaceX {

public:
 Implementation();
 virtual void bootloader_done();

private:
    InterfaceCaller _caller;
};


#endif