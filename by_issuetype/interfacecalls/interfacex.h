#include "stdio.h"

#ifndef INTERFACEX_H
#define INTERFACEX_H

// interface with pure virtual methods, no implementation here
class InterfaceX {
   public:
       virtual void bootloader_done() = 0;
};

#endif