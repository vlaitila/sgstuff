#include "interfacex.h"

#ifndef INTERFACECALLER_H
#define INTERFACECALLER_H

// user of the implementation via interface
class InterfaceCaller {

 public:
    InterfaceCaller(InterfaceX* blah_to_call) : _blah(blah_to_call) { }
    void dummy();

 private:
   // note: pointer is for interface, not implementation
   InterfaceX *_blah;
};

#endif