#include "implementation.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void Implementation::bootloader_done() {
  double inputValue = atof("123423");
  double outputValue = sqrt(inputValue);
  fprintf(stdout,"The square root of %g is %g\n",
          inputValue, outputValue);
  

}

Implementation::Implementation(): _caller(this) {
    // impl2
}

// Body here