#include "interfacecaller.h"
#include "implementation.h"

void InterfaceCaller::dummy() {
  // this call was not visible on Implementation.cpp
  _blah->bootloader_done();

}

