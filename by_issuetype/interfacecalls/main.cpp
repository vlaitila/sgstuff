#include "interfacecaller.h"
#include "implementation.h"

int main (int argc, char *argv[])
{
  InterfaceX* impl = new Implementation();
  InterfaceCaller c(impl);
  c.dummy();
  return 0;
}
