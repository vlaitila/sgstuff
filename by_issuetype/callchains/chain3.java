

public class chain3 implements chainapi {

    chain4 association = new chain4();

    public int jchainsecondcalled() {
        return new chain4().jchainend();
    }

    public int othermember() {
        return 2;
    }

    @Override
    public int apicall() {
        return chain4().jchainend();
    }
}