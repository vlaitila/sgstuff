����
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MySample.Services
{
    [Serializable]
    [DataContract]
    public class MResponse
    {
        [DataMember]
        public List<Sample> Samples { get; set; }

        [DataMember]
        public int TotalRowCount { get; set; }
    }
}
