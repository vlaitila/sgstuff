package funcoverloading;

public class ImpossibleCall {
    public ImpossibleCall() {

    }

    public void functionThatHasInvalidCall() {
        OverloadedFuncsJavaDifferentVisibility o = new OverloadedFuncsJavaDifferentVisibility();
        int x = o.overloadedfunc2("asdf ");
    }

}