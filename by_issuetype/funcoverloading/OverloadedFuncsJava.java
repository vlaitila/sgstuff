package funcoverloading;

import java.util.*;


public class OverloadedFuncsJava {


    public int overloadedfunc(String s) {
        return 23+1;
    }


    public int overloadedfunc(String s, String s2) {
        return 23 + 1 + 34;
    }

    public void funcThatCallsOneParameterVersion() {
        System.out.println("testing");
        overloadedfunc("asdf");
    }


    public void funcThatCallsTwoParameterVersion() {
        System.out.println("testing");
        overloadedfunc("asdf", "asdf2");
    }

}


