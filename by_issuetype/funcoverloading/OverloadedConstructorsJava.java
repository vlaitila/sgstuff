package funcoverloading;

import java.util.*;


public class OverloadedConstructorsJava {

    public OverloadedConstructorsJava(String s) {
        // no impl required
    }

    public OverloadedConstructorsJava(String s, String s2) {
        // no impl required
    }

    public static void funcThatCallsOneParameterVersion() {
        System.out.println("testing");
        new OverloadedConstructorsJava("asdf");
    }


    public void funcThatCallsTwoParameterVersion() {
        System.out.println("testing");
        new OverloadedConstructorsJava("asdf", "asdf2");
    }

}


